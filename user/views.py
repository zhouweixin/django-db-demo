from django.shortcuts import render,redirect
from django.db import connection


def get_curor():
    return connection.cursor()


def index(request):
    cursor = get_curor()
    cursor.execute("select id, name, age from user")
    users = cursor.fetchall()
    return render(request, 'index.html', context={'users': users})


def add(request):
    name = request.POST.get("name")
    age = request.POST.get("age")

    cursor = get_curor()
    sql = "insert into user (id, name, age) values (null, '%s', %s)" % (name, age)
    print("="*30)
    print(sql)
    print("="*30)
    cursor.execute(sql)
    return redirect('index')
