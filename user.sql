create database if not exists django_db default character set utf8 collate utf8_general_ci;

use django_db;
create table if not exists user(
    id int auto_increment primary key,
    name varchar(100),
    age int
);

insert into user (id, name, age) values (null, '刘备', 50);
insert into user (id, name, age) values (null, '关羽', 48);
insert into user (id, name, age) values (null, '张飞', 45);

select id, name, age from user;
